import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static char[] DIRECTION = { 'Z', 'Q', 'S', 'D' };
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Maze maze = new Maze();

        while (!maze.isSolved()) {
            maze.display();
            Direction direction = askForDirection();
            maze.movePlayer(direction);
        }

        System.out.println("Well played");
    }

    static Direction askForDirection() {
        char direction = '0';
        while (direction != 'Z' && direction != 'Q' && direction != 'S' && direction != 'D') {
            System.out.print("Enter a direction (Z - Q - S - D) and press enter : ");
            direction = scanner.next().toUpperCase().charAt(0);
        }

        switch (direction) {
            case 'Z':
                return Direction.UP;
            case 'Q':
                return Direction.LEFT;
            case 'S':
                return Direction.DOWN;
            case 'D':
                return Direction.RIGHT;
            default:
                throw new NullPointerException("Unreachable code");
        }
    }
}
