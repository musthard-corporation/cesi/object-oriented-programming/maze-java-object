public class Position {
    int row;
    int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public boolean match(int row, int column) {
        return this.row == row && this.column == column;
    }

    public boolean match(Position position) {
        return this.row == position.row && this.column == position.column;
    }
}
