import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Maze {
    static char PLAYER = 'P';
    static char EXIT = 'E';
    static char WALL = '#';
    static char ROAD = ' ';

    int size;
    Position player;
    Position exit;
    List<Position> walls;

    public Maze() {
        this.size = 5;
        this.player = new Position(0, 0);
        this.exit = new Position(size - 1, size - 1);
        this.walls = Arrays.asList(
            new Position(1, 0),
            new Position(1, 1),
            new Position(1, 2),
            new Position(1, 3),
            new Position(3, 0),
            new Position(3, 2),
            new Position(3, 3),
            new Position(3, 4)
        );
    }

    public Maze(int size) {
        this.size = size;
        this.player = new Position(0, 0);
        this.exit = new Position(size - 1, size - 1);
        this.walls = new ArrayList<>();
    }

    public void display() {
        for (int row = 0; row < this.size; row++) {
            for (int column = 0; column < this.size; column++) {
                System.out.print("|");
                if (this.player.match(row, column)) {
                    System.out.print(PLAYER);
                } else if (this.exit.match(row, column)) {
                    System.out.print(EXIT);
                } else if (this.isAnyWallForPosition(row, column)) {
                    System.out.print(WALL);
                } else {
                    System.out.print(ROAD);
                }
            }
            System.out.println("|");
        }
    }

    public void movePlayer(Direction direction) {
        switch (direction) {
            case Direction.UP:
                if (this.player.row > 0 && !this.isAnyWallForPosition(this.player.row - 1, this.player.column)) {
                    this.player.row--;
                }
                break;
            case Direction.LEFT:
                if (this.player.column > 0 && !this.isAnyWallForPosition(this.player.row, this.player.column - 1)) {
                    this.player.column--;
                }
                break;
            case Direction.RIGHT:
                if (this.player.column < this.size
                        - 1 && !this.isAnyWallForPosition(this.player.row, this.player.column + 1)) {
                    this.player.column++;
                }
                break;
            case Direction.DOWN:
                if (this.player.row < this.size - 1
                        && !this.isAnyWallForPosition(this.player.row + 1, this.player.column)) {
                    this.player.row++;
                }
                break;
        }
    }

    public boolean isSolved() {
        return this.player.match(this.exit);
    }

    private boolean isAnyWallForPosition(int row, int column) {
        return this.walls.stream().anyMatch(w -> w.match(row, column));
    }
}
